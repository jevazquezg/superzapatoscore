﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class MyDBContext : DbContext
    {

        #region Atributos

        public DbSet<articles> articles { get; set; }
        public DbSet<stores> stores { get; set; }

        #endregion

        #region Constructores

        public MyDBContext(DbContextOptions<MyDBContext> options) : base(options) { }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<articles>().Property("price").HasPrecision(18, 2);
            base.OnModelCreating(modelBuilder);
        }

        #endregion


    }
}
