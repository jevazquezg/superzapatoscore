﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class stores
    {
        /// <summary>
        /// ID de la tienda
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Nombre de la tienda
        /// </summary>
        [Required]
        [StringLength(100)]
        public string name { get; set; }

        /// <summary>
        /// Dirección de la tienda
        /// </summary>
        public string address { get; set; }
    }
}
