﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class articles
    {
        /// <summary>
        /// ID del artìculo
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Nombre del artículo
        /// </summary>
        [Required]
        [StringLength(100)]
        public string name { get; set; }

        /// <summary>
        /// Descripción del artículo
        /// </summary>
        [StringLength(250)]
        public string description { get; set; }

        /// <summary>
        /// Precio del artículo
        /// </summary>

        public decimal price { get; set; }

        /// <summary>
        /// Total en mostrador
        /// </summary>
        public int total_in_shelf { get; set; }

        /// <summary>
        /// Total en bodega
        /// </summary>
        public int total_in_vault { get; set; }

        /// <summary>
        /// Tienda donde se encuentra el artículo
        /// </summary>
        public int store_id { get; set; }
    }
}
