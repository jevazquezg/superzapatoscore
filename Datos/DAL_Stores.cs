﻿using Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    /// <summary>
    /// Capa de Acceso a Datos (DAL) para la entidad Stores
    /// </summary>
    public class DAL_Stores
    {
        private readonly MyDBContext _context;

        public DAL_Stores(MyDBContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Obtiene el listado de tiendas de la DB
        /// </summary>
        public List<stores> ListStores()
        {
            using (var db = _context)
            {
                return db.stores.ToList();
            }
        }

        /// <summary>
        /// Obtiene la tienda de la DB
        /// </summary>
        public stores StoreById(int id)
        {
            using (var db = _context)
            {
                return db.stores.SingleOrDefault(b => b.id == id);
            }
        }

        /// <summary>
        /// Agrega o Actualiza un registro en la taba Stores de la DB
        /// </summary>
        /// <param name="store">Registro a agregar o editar</param>
        /// <param name="query">Tipo de acción 1: Insert, 2:Update</param>
        public bool AddOrEditStore(stores store, Query query)
        {
            bool flag = false;

            using (var db = _context)
            {
                switch (query)
                {
                    case Query.Insert:

                        db.stores.Add(store);
                        db.SaveChanges();

                        flag = true;

                        break;

                    case Query.Update:

                        var result = db.stores.SingleOrDefault(b => b.id == store.id);
                        if (result != null)
                        {
                            result.name = store.name;
                            result.address = store.address;
                            db.SaveChanges();

                            flag = true;
                        }
                        else
                        {
                            flag = false;
                        }

                        break;
                }

                return flag;
            }
        }

        /// <summary>
        /// Elimina un registro en la taba Stores de la DB
        /// </summary>
        public bool DeleteStore(int id)
        {
            bool flag = false;

            using (var db = _context)
            {
                var result = db.stores.SingleOrDefault(b => b.id == id);
                if (result != null)
                {
                    db.stores.Remove(result);
                    db.SaveChanges();

                    flag = true;
                }
                else
                {
                    flag = false;
                }


                return flag;
            }
        }

    }
}
