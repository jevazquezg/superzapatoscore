﻿using Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    /// <summary>
    /// Capa de Acceso a Datos (DAL) para la entidad Articles
    /// </summary>
    public class DAL_Articles
    {

        private readonly MyDBContext _context;

        public DAL_Articles(MyDBContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Obtiene el listado de atículos de la DB
        /// </summary>
        public List<articles> ListArticles()
        {
            using (var db = _context)
            {
                return db.articles.ToList();
            }
        }

        /// <summary>
        /// Obtiene el artículo de la DB
        /// </summary>
        public articles ArticleById(int id)
        {
            using (var db = _context)
            {
                return db.articles.SingleOrDefault(b => b.id == id);
            }
        }

        /// <summary>
        /// Obtiene el atículo de la DB segun el id de tienda
        /// </summary>
        public List<articles> getArticleByStoreID(int id)
        {
            using (var db = _context)
            {
                return db.articles.Where(b => b.store_id == id).ToList();
            }
        }

        /// <summary>
        /// Agrega o Actualiza un registro en la taba Articles de la DB
        /// </summary>
        /// <param name="article">Registro a agregar o editar</param>
        /// <param name="query">Tipo de acción 1: Insert, 2:Update</param>
        public bool AddOrEditArticle(articles article, Query query)
        {
            bool flag = false;

            using (var db = _context)
            {
                switch (query)
                {
                    case Query.Insert:

                        db.articles.Add(article);
                        db.SaveChanges();

                        flag = true;

                        break;

                    case Query.Update:

                        var result = db.articles.SingleOrDefault(b => b.id == article.id);
                        if (result != null)
                        {
                            result.name = article.name;
                            result.description = article.description;
                            result.price = article.price;
                            result.total_in_shelf = article.total_in_shelf;
                            result.total_in_vault = article.total_in_vault;
                            result.store_id = article.store_id;

                            db.SaveChanges();

                            flag = true;
                        }
                        else
                        {
                            flag = false;
                        }

                        break;
                }

                return flag;
            }
        }

        /// <summary>
        /// Elimina un registro en la taba Articles de la DB
        /// </summary>
        public bool DeleteArticle(int id)
        {
            bool flag = false;

            using (var db = _context)
            {
                var result = db.articles.SingleOrDefault(b => b.id == id);
                if (result != null)
                {
                    db.articles.Remove(result);
                    db.SaveChanges();

                    flag = true;
                }
                else
                {
                    flag = false;
                }


                return flag;
            }
        }

    }
}
