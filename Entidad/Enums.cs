﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad
{
    /// <summary>
    /// Enum Respuestas de NE
    /// </summary>
    public enum Respuesta
    {
        // Codigo general de que todo estuvio bien
        Ok = 0,
        // Codigo general de error
        Error = 1
    }

    /// <summary>
    /// Enum para query
    /// </summary>
    public enum Query
    {
        Insert = 1,
        Update = 2,
        Select = 3,
        Delete = 4,
        BajaLogica = 5
    }
}
