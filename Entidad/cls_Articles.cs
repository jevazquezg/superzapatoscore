﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad
{
    public class cls_Articles
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public decimal price { get; set; }
        public int total_in_shelf { get; set; }
        public int total_in_vault { get; set; }
        public int store_id { get; set; }


        public cls_Articles() { }

        public cls_Articles(int pId, string pName, string pDescription, decimal pPrice, int pTotal_in_shelf, int pTotal_in_vault, int pStore_id)
        {
            id = pId;
            name = pName;
            description = pDescription;
            price = pPrice;
            total_in_shelf = pTotal_in_shelf;
            total_in_vault = pTotal_in_vault;
            store_id = pStore_id;
        }
    }
}
