﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad
{
    public class cls_Stores
    {

        public int? id { get; set; }

        public string name { get; set; }

        public string address { get; set; }


        public cls_Stores() { }

        public cls_Stores(int pId, string pName, string pAddress)
        {
            id = pId;

            name = pName;

            address = pAddress;
        }
    }
}
