﻿using Datos;
using Entidad;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Negocio;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text.Json;
using System.Threading.Tasks;
using Utilerias;

namespace SuperZapatosApi.Controllers
{

    [ApiController]
    [EnableCors("AllowOrigin")]
    //[TypeFilter(typeof(ExceptionManager))]
    public class ServiceController : ControllerBase
    {
        private readonly MyDBContext _context;
        public ServiceController(MyDBContext context)
        {
            _context = context;
        }

        #region Stores

        [HttpGet("services/stores")]
        public cls_Response getStores()
        {
            cls_Response oResponse = new cls_Response();

            try
            {
                NE_Stores neStores = new NE_Stores(_context);
                List<cls_Stores> responseStores = neStores.ListStores();

                // Genera la estructura de respuesta
                oResponse.stores = responseStores;
                oResponse.total_elements = responseStores.Count;
                oResponse.success = true;
            }
            catch (System.Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            //return JsonSerializer.Serialize<cls_Response>(oResponse, options);
            return oResponse;
        }

        [HttpPost("services/store")]
        public cls_Response setStore([FromBody] cls_Stores body)
        {

            cls_Response oResponse = new cls_Response();

            try
            {
                NE_Stores neStores = new NE_Stores(_context);
                Respuesta respuesta = neStores.AddOrEditStore(body);

                // Genera la estructura de respuesta
                switch (respuesta)
                {
                    case Respuesta.Ok:
                        oResponse.success = true;
                        break;
                    case Respuesta.Error:
                        oResponse.success = false;
                        oResponse.error_mesg = "Bad request";
                        oResponse.error_code = 400;
                        break;
                }

            }
            catch (System.Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            return oResponse;
        }

        [HttpGet("services/store/{id}")]
        public cls_Response getStore([FromRoute] int id)
        {
            cls_Response oResponse = new cls_Response();

            try
            {
                if (id.ParseIntNull() != null)
                {
                    NE_Stores neStores = new NE_Stores(_context);
                    cls_Stores responseStores = neStores.StoreByID(id.ParseInt());

                    // Genera la estructura de respuesta
                    if (responseStores != null)
                    {
                        oResponse.store = responseStores;
                        oResponse.success = true;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.error_mesg = "Record not Found";
                        oResponse.error_code = 404;
                    }
                }
                else
                {
                    oResponse.success = false;
                    oResponse.error_mesg = "Bad Request";
                    oResponse.error_code = 400;
                }

            }
            catch (System.Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            return oResponse;
        }

        [HttpDelete("services/store/{id}")]
        public cls_Response delStore([FromRoute] int id)
        {
            cls_Response oResponse = new cls_Response();

            try
            {
                if (id.ParseIntNull() != null)
                {
                    NE_Stores neStores = new NE_Stores(_context);
                    Respuesta respuesta = neStores.DeleteStore(id.ParseInt());

                    // Genera la estructura de respuesta
                    switch (respuesta)
                    {
                        case Respuesta.Ok:
                            oResponse.success = true;
                            break;
                        case Respuesta.Error:
                            oResponse.success = false;
                            oResponse.error_mesg = "Bad request";
                            oResponse.error_code = 400;
                            break;
                    }
                }
                else
                {
                    oResponse.success = false;
                    oResponse.error_mesg = "Bad Request";
                    oResponse.error_code = 400;
                }
            }
            catch (System.Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            return oResponse;
        }

        #endregion

        #region Articles

        [HttpGet("services/articles")]
        public cls_Response getArticles()
        {
            cls_Response oResponse = new cls_Response();

            try
            {
                NE_Articles neArticles = new NE_Articles(_context);
                List<cls_Articles> responseArticles = neArticles.ListArticles();

                // Genera la estructura de respuesta
                oResponse.articles = responseArticles;
                oResponse.total_elements = responseArticles.Count;
                oResponse.success = true;
            }
            catch (System.Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            return oResponse;
        }

        [HttpGet("services/article/{id}")]
        public cls_Response getArticle([FromRoute] int id)
        {
            cls_Response oResponse = new cls_Response();

            try
            {
                if (id.ParseIntNull() != null)
                {
                    NE_Articles nArticles = new NE_Articles(_context);
                    cls_Articles responseArticles = nArticles.ArticleByID(id.ParseInt());

                    // Genera la estructura de respuesta
                    if (responseArticles != null)
                    {
                        oResponse.article = responseArticles;
                        oResponse.success = true;
                    }
                    else
                    {
                        oResponse.success = false;
                        oResponse.error_mesg = "Record not Found";
                        oResponse.error_code = 404;
                    }
                }
                else
                {
                    oResponse.success = false;
                    oResponse.error_mesg = "Bad Request";
                    oResponse.error_code = 400;
                }
            }
            catch (System.Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            return oResponse;
        }

        [HttpGet("services/articles/stores/{id}")]
        public cls_Response getArticleByStoreId([FromRoute] int id)
        {
            cls_Response oResponse = new cls_Response();

            try
            {
                if (id.ParseIntNull() != null)
                {
                    NE_Articles neArticles = new NE_Articles(_context);
                    List<cls_Articles> responseArticles = neArticles.getArticleByStoreID(id.ParseInt());

                    // Genera la estructura de respuesta
                    oResponse.articles = responseArticles;
                    oResponse.total_elements = responseArticles.Count;
                    oResponse.success = true;
                }
                else
                {
                    oResponse.success = false;
                    oResponse.error_mesg = "Bad Request";
                    oResponse.error_code = 400;
                }
            }
            catch (System.Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            return oResponse;
        }

        [HttpPost("services/article")]
        public cls_Response setArticle([FromBody] cls_Articles article)
        {
            cls_Response oResponse = new cls_Response();

            try
            {
                NE_Articles neArticles = new NE_Articles(_context);
                Respuesta respuesta = neArticles.AddOrEditArticles(article);

                // Genera la estructura de respuesta
                switch (respuesta)
                {
                    case Respuesta.Ok:
                        oResponse.success = true;
                        break;
                    case Respuesta.Error:
                        oResponse.success = false;
                        oResponse.error_mesg = "Bad request";
                        oResponse.error_code = 400;
                        break;
                }
            }
            catch (System.Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            return oResponse;
        }

        [HttpDelete("services/article/{id}")]
        public cls_Response delArticle([FromRoute] int id)
        {
            cls_Response oResponse = new cls_Response();

            try
            {
                if (id.ParseIntNull() != null)
                {
                    NE_Articles neArticles = new NE_Articles(_context);
                    Respuesta respuesta = neArticles.DeleteArticle(id.ParseInt());

                    // Genera la estructura de respuesta
                    switch (respuesta)
                    {
                        case Respuesta.Ok:
                            oResponse.success = true;
                            break;
                        case Respuesta.Error:
                            oResponse.success = false;
                            oResponse.error_mesg = "Bad request";
                            oResponse.error_code = 400;
                            break;
                    }
                }
                else
                {
                    oResponse.success = false;
                    oResponse.error_mesg = "Bad Request";
                    oResponse.error_code = 400;
                }
            }
            catch (System.Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            return oResponse;
        }

        #endregion
    }
}
