﻿using Datos;
using Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    /// <summary>
    /// Capa de Negocio para la entidad Articles
    /// </summary>
    public class NE_Articles
    {
        private readonly MyDBContext _context;

        public NE_Articles(MyDBContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Listado de artículos
        /// </summary>
        public List<cls_Articles> ListArticles()
        {
            List<cls_Articles> clsArticles = new List<cls_Articles>();

            DAL_Articles dalArticles = new DAL_Articles(_context);
            List<articles> articles = dalArticles.ListArticles();

            try
            {
                foreach (var article in articles)
                {
                    clsArticles.Add(new cls_Articles(article.id, article.name, article.description, article.price, article.total_in_shelf, article.total_in_vault, article.store_id));
                }
            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Se envia la excepcion al controlador para armar la respuesta
                throw ex;
            }

            return clsArticles;
        }

        /// <summary>
        /// Consulta artículo por ID
        /// </summary>
        public cls_Articles ArticleByID(int id)
        {
            cls_Articles clsArticle = null;

            DAL_Articles dalArticles = new DAL_Articles(_context);
            articles articles = dalArticles.ArticleById(id);

            try
            {
                if (articles != null)
                {
                    clsArticle = new cls_Articles(articles.id, articles.name, articles.description, articles.price, articles.total_in_shelf, articles.total_in_vault, articles.store_id);
                }
            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Se envia la excepcion al controlador para armar la respuesta
                throw ex;
            }

            return clsArticle;
        }

        /// <summary>
        /// Consulta artículo por ID de tienda
        /// </summary>
        public List<cls_Articles> getArticleByStoreID(int id)
        {
            List<cls_Articles> clsArticles = new List<cls_Articles>();

            DAL_Articles dalArticles = new DAL_Articles(_context);
            List<articles> articles = dalArticles.getArticleByStoreID(id);

            try
            {
                foreach (var article in articles)
                {
                    clsArticles.Add(new cls_Articles(article.id, article.name, article.description, article.price, article.total_in_shelf, article.total_in_vault, article.store_id));
                }
            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Se envia la excepcion al controlador para armar la respuesta
                throw ex;
            }

            return clsArticles;
        }

        /// <summary>
        /// Agrega o Actualiza un registro en la tabla Articles
        /// </summary>
        /// <param name="article"></param>
        /// <returns></returns>
        public Respuesta AddOrEditArticles(cls_Articles article)
        {
            if (article != null)
            {
                DAL_Articles dalArticles = new DAL_Articles(_context);
                bool respuesta = dalArticles.AddOrEditArticle(new articles()
                {
                    id = article.id.HasValue ? (int)article.id : 0,
                    name = article.name,
                    description = article.description,
                    price = article.price,
                    total_in_shelf = article.total_in_shelf,
                    total_in_vault = article.total_in_vault,
                    store_id = article.store_id
                }, article.id.HasValue ? Query.Update : Query.Insert);

                if (respuesta)
                {
                    return Respuesta.Ok;
                }
                else
                {
                    return Respuesta.Error;
                }
            }
            else
            {
                return Respuesta.Error;
            }
        }

        /// <summary>
        /// Elimina un registro en la taba Articles
        /// </summary>
        public Respuesta DeleteArticle(int? id)
        {
            if (id != null)
            {
                DAL_Articles dalArticles = new DAL_Articles(_context);
                bool respuesta = dalArticles.DeleteArticle((int)id);

                if (respuesta)
                {
                    return Respuesta.Ok;
                }
                else
                {
                    return Respuesta.Error;
                }
            }
            else
            {
                return Respuesta.Error;
            }
        }
    }
}
