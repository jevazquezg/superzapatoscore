﻿using Datos;
using Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    /// <summary>
    /// Capa de Negocio para la entidad Stores
    /// </summary>
    public class NE_Stores
    {
        private readonly MyDBContext _context;

        public NE_Stores(MyDBContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Listado de tiendas
        /// </summary>
        public List<cls_Stores> ListStores()
        {
            List<cls_Stores> clsStores = new List<cls_Stores>();

            DAL_Stores dalStores = new DAL_Stores(_context);
            List<stores> stores = dalStores.ListStores();

            try
            {
                foreach (var store in stores)
                {
                    clsStores.Add(new cls_Stores(store.id, store.name, store.address));
                }
            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Se envia la excepcion al controlador para armar la respuesta
                throw ex;
            }

            return clsStores;
        }

        /// <summary>
        /// Consulta tienda por ID
        /// </summary>
        public cls_Stores StoreByID(int id)
        {
            cls_Stores clsStore = null;

            DAL_Stores dalStores = new DAL_Stores(_context);
            stores stores = dalStores.StoreById(id);

            try
            {
                if (stores != null)
                {
                    clsStore = new cls_Stores(stores.id, stores.name, stores.address);
                }
            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Se envia la excepcion al controlador para armar la respuesta
                throw ex;
            }

            return clsStore;
        }

        /// <summary>
        /// Agrega o Actualiza un registro en la tabla Stores
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public Respuesta AddOrEditStore(cls_Stores store)
        {
            if (store != null)
            {
                DAL_Stores dalStores = new DAL_Stores(_context);
                bool respuesta = dalStores.AddOrEditStore(new stores()
                {
                    id = store.id.HasValue ? (int)store.id : 0,
                    name = store.name,
                    address = store.address
                },
                    store.id.HasValue ? Query.Update : Query.Insert);

                if (respuesta)
                {
                    return Respuesta.Ok;
                }
                else
                {
                    return Respuesta.Error;
                }
            }
            else
            {
                return Respuesta.Error;
            }
        }

        /// <summary>
        /// Elimina un registro en la taba Stores
        /// </summary>
        public Respuesta DeleteStore(int? id)
        {
            if (id != null)
            {
                DAL_Stores dalStores = new DAL_Stores(_context);
                bool respuesta = dalStores.DeleteStore((int)id);

                if (respuesta)
                {
                    return Respuesta.Ok;
                }
                else
                {
                    return Respuesta.Error;
                }
            }
            else
            {
                return Respuesta.Error;
            }
        }
    }
}
