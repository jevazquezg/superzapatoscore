﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilerias
{
    public static class Extensions
    {
        /// <summary>
        /// EXTENSION PARA PODER CONVERTIR UN VALOR STRING A UN VALOR ENTERO
        /// </summary>
        /// <param name="pValor"></param>
        /// <returns></returns>
        public static int ParseInt(this string sCadena)
        {
            var iValorEntero = 0;
            return !int.TryParse(sCadena, out iValorEntero)
                    ? 0
                    : iValorEntero;
        }

        /// <summary>
        /// EXTENSION PARA PODER CONVERTIR UN VALOR STRING A UN VALOR ENTERO NULL
        /// </summary>
        /// <param name="pValor"></param>
        /// <returns></returns>
        public static int? ParseIntNull(this string sCadena)
        {
            var iValorEntero = 0;
            return !int.TryParse(sCadena, out iValorEntero)
                    ? (int?)null
                    : iValorEntero;
        }

        /// <summary>
        /// EXTENSION PARA PODER CONVERTIR UN VALOR STRING A UN VALOR ENTERO
        /// </summary>
        /// <param name="pValor"></param>
        /// <returns></returns>
        public static int ParseInt(this object sCadena)
        {
            var iValorEntero = 0;
            return !int.TryParse(sCadena.ToString(), out iValorEntero)
                    ? 0
                    : iValorEntero;
        }

        /// <summary>
        /// EXTENSION PARA PODER CONVERTIR UN VALOR STRING A UN VALOR ENTERO NULL
        /// </summary>
        /// <param name="pValor"></param>
        /// <returns></returns>
        public static int? ParseIntNull(this object sCadena)
        {
            var iValorEntero = 0;
            return !int.TryParse(sCadena.ToString(), out iValorEntero)
                    ? (int?)null
                    : iValorEntero;
        }

        /// <summary>
        /// EXTENSION PARA PODER CONVERTIR UN VALOR STRING A UN VALOR DECIMAL
        /// </summary>
        /// <param name="pValor"></param>
        /// <returns></returns>
        public static decimal ParseDecimal(this string sCadena)
        {
            var dcValorEntero = 0m;
            return !decimal.TryParse(sCadena, out dcValorEntero)
                    ? 0m
                    : dcValorEntero;
        }

    }
}
